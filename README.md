This is a python module for generating cohort-analysis.csv by using order.cvs and customer.csv files.

## Installation & Configuration 
 - Follow these steps to setup your environment on Mac OS
    - Open your terminal
    - If you don't have pyenv, install it using ` brew install pyenv`
    - List all versions of python using `pyevn versions`
    - If you don't see Python 3.4.4. Install Python 3.4.4 by typing 
        `pyenv install 3.4.4` 
    - Switch your global Python to 3.4.4 using `pyenv global 3.4.4`
    - Check your Python and PIP version by typing following commands:
        - `python -V`
        - `pip -V`
    - Now create a Python 3 virtualenv.
        - If you don't have *virtualenv*, use following command to install it
            - `pip install virtualenv`
            - You may need to rehash your pyenv. `pyenv rehash`
        - Create a virtualenv:  `virtualenv ~/.virtualenvs/myenv` (choose any name you like)
        - Activate virtualenv: `source ~/.virtualenvs/myenv/bin/activate`
        
    - Go to project directory (if you are not already there) 
    - Install required libraries using `pip install -r requirements.txt`
    
 You should be good to go now.
 
 ## Generating customer.csv & order.csv Files
 You can generate sample customer.csv and order.csv files using `input_data/generate_input_files.py`script. 
 ```
 >python input_data/generate_input_files.py '2015-01-01' -nc 8 -c 100
 ```
 This script generate random data for both customers and orders using following arguments:
 - **start_date** (positional arg) - Cohort start date. All the signup and order date will start from this date. This date will be in UTC timezone.
 - **num-cohort** (optional arg) - `--num-cohort` or `-nc`, number of cohorts for which we need to generate sample data. By default it is set to 8.
 - **customers** (optional arg) - `--customers` or `-c`, number of customer you want to generate.
 
 ## Generating cohort-analysis.csv
 To generate cohort-analysis.csv file, use `cohort_analysis.py` script.
 ```
 >python cohort_analysis.py '2015-01-01' -ccfp 'input_data/customer.csv' -ocfp 'input_data/order.csv'
 ```
 
 Here is the more detail about the arguments:
 
 - Positional arguments:
    - `start_date`:            Cohort starting date
 
 - Optional arguments:   
   - `-ccfp` or `--customer-csv-path` : Full path of customer csv file (default is set to **customer.csv**)
   - `-ocfp` or `--order-csv-path`: Full path of order csv file (default is set to **order.csv**)
   - `-ofn` or `--output-file-name`: Full path of output csv file (default is set to **cohort_analysis.csv**)
   - `-bi` or `--bucket-interval`: Cohort bucket interval (default is **7** Days)
   - `-nc` or `--num-cohort`: Number of cohorts (should be at least **8**)
   - `-tz` or `--timezone`: Timezone for which we are analysing the results (default is set to **UTC**)

## Steps to Test the Module
- Run **generate_input_files.py** to create customer.csv and order.csv files 
    ```
     >python input_data/generate_input_files.py '2015-01-01' -nc 8 -c 100
     ```
- Run **cohort_analysis.py** script to generate cohort-analysis.csv
    ```
     >python cohort_analysis.py '2015-01-01' -ccfp 'input_data/customer.csv' -ocfp 'input_data/order.csv'
     ```
