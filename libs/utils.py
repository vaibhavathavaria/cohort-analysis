import os
import tarfile

from datetime import datetime
from dateutil import tz, zoneinfo

UTC_TIMEZONE = 'UTC'


def get_datetime_from_date_str(date_text, timezone=UTC_TIMEZONE):
    """
    Covert date string to datetime.datetime object
    """
    tz_info = tz.gettz(timezone)
    try:
        datetime_obj = datetime.strptime(date_text + " 00:00:00", '%Y-%m-%d %H:%M:%S')
    except ValueError:
        raise ValueError("Incorrect date format, should be YYYY-MM-DD")

    return datetime_obj.replace(tzinfo=tz_info)


def get_datetime_from_datetime_str(datetime_str, timezone=UTC_TIMEZONE):
    """
    Covert datetime string to datetime.datetime object
    """
    tz_info = tz.gettz(timezone)
    try:
        datetime_obj = datetime.strptime(datetime_str, '%Y-%m-%d %H:%M:%S')
    except ValueError:
        raise ValueError("Incorrect date format, should be YYYY-MM-DD HH:MM:SS")

    return datetime_obj.replace(tzinfo=tz_info)


def valid_timezone(timezone_str):
    """
    Checks if the timezone string is a valid timezone
    :param timezone_str:
    :return: Boolean
    """
    zi_path = os.path.abspath(os.path.dirname(zoneinfo.__file__))
    zone_files = tarfile.TarFile.open(os.path.join(zi_path, 'dateutil-zoneinfo.tar.gz'))
    zone_names = zone_files.getnames()

    if timezone_str not in zone_names:
        return False

    return True
