import argparse
import csv
import copy
import os
from math import floor
from datetime import datetime, timedelta
from dateutil import tz
from collections import OrderedDict
from libs import utils


COHORT_COUNT = 8
COHORT_INTERVAL_TYPE_WEEK = 7
COHORT_BUCKET_COUNT = 8
COHORT_BUCKET_INTERVAL = 7


KEY_NUM_CUSTOMERS = 'num_customers'
KEY_FIRST_ORDERS = 'first_orders'
KEY_TOTAL_ORDERS = 'total_orders'
KEY_COHORT_LABEL = 'cohort_label'
KEY_COHORT_BUCKETS = 'cohort_buckets'


class CohortAnalysis:
    def __init__(
            self, customer_csv_path, order_csv_path,
            output_file_name, start_date,
            num_cohort=COHORT_COUNT,
            cohort_interval=COHORT_INTERVAL_TYPE_WEEK,
            num_buckets=COHORT_BUCKET_COUNT,
            bucket_interval=COHORT_BUCKET_INTERVAL,
            timezone=utils.UTC_TIMEZONE
    ):
        """
        :param customer_csv_path: (string) file path with name
        :param order_csv_path: (string) file path with name
        :param output_file_name: (string) file path with name
        :param start_date: (datetime.date) cohort start date
        :param bucket_interval: (int) cohort bucket size
        :param num_cohort: (int) number of cohort
        :param timezone: (string) timezone for grouping results
        """
        self._customer_csv_path = customer_csv_path
        self._order_csv_path = order_csv_path
        self._output_file_name = output_file_name
        self._start_date = start_date
        self._bucket_interval = bucket_interval
        self._num_cohorts = num_cohort
        self._cohort_interval = cohort_interval
        self._num_buckets = num_buckets
        self._tz_info = tz.gettz(timezone)

        # Calculate cohort analysis last date, we will not use
        # any customer signup or order after this date in our analysis
        self._cohort_last_date = self._start_date + timedelta(
            days=self._bucket_interval*self._num_buckets
        )

        self._cohort_result = self._initialize_cohort_result_dict()

    def _load_customer_csv(self):
        """
        Loads customer information from a csv file.
        Expected csv format (employee_id (int), signup_date (datetime))

        :return: OrderedDict in the following format
        {
            "001" : {
                "signup_date": datetime("2014-04-23 03:42:53")
            },
            "011" : {
                "signup_date": datetime("2015-03-30 06:21:27")
            },
            ...
        }
        """
        customer_dict = OrderedDict()
        with open(self._customer_csv_path, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader, None)  # skip the headers
            for row in csv_reader:
                (customer_id, signup_date) = row
                # Convert signup_date to given timezone from UTC
                utc_signup_date = utils.get_datetime_from_datetime_str(signup_date)
                local_signup_date = utc_signup_date.astimezone(self._tz_info)
                customer_dict[customer_id] = local_signup_date

        return customer_dict

    def _load_order_csv(self):
        """
        Loads customer's order information from a csv file.
        Expected csv format (employee_id (int), order_date (datetime))

        :return: OrderedDict in the following format
        {
            "001" : {
                "order_date": [datetime("2014-04-23 20:34:23"), datetime("2014-11-04: 23:21:23")]
            },
            "090" : {
                "order_date": [datetime("2016-01-23 04:52:22")]
            },
            ...
        }
        """
        order_dict = OrderedDict()
        with open(self._order_csv_path, 'r') as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader, None)  # skip the headers
            for row in csv_reader:
                (customer_id, order_date) = row
                # convert order_date to given timezone
                utc_order_date = utils.get_datetime_from_datetime_str(order_date)
                local_order_date = utc_order_date.astimezone(self._tz_info)
                if customer_id in order_dict:
                    order_dict[customer_id].append(local_order_date)
                else:
                    order_dict[customer_id] = [local_order_date]
        return order_dict

    def _create_cohort_bucket_dict(self, bucket_count):
        """
        This method creates empty bucket dictionaries for each cohort
        equal to bucket_count
        :param bucket_count: Number of buckets in a cohort
        :return: Dict
        """
        cohort_dict = {}
        for bucket_index in range(bucket_count):
            cohort_dict[bucket_index] = {
                KEY_FIRST_ORDERS: 0,
                KEY_TOTAL_ORDERS: 0,
            }
        return cohort_dict

    def _initialize_cohort_result_dict(self):
        """
        Initialize an OrderedDict to store cohort result. OrderedDict keys are date string,
        which represent start_date of each cohort
        :return: OrderedDict, it should be in following format
        {
            '2015-01-01': {
                KEY_COHORT_LABEL : '2015/01/01 - 2015/01/06',
                KEY_NUM_CUSTOMERS : 0,
                KEY_COHORT_BUCKETS : {..}
            },
            ...
        }
        """
        cohort_result_dict = OrderedDict()
        bucket_count = self._num_buckets
        for cnt in range(self._num_cohorts):
            cohort_start_date = self._start_date + timedelta(days=self._bucket_interval * cnt)
            # As cohort start_date is included in the range that's why we will add
            # (self._cohort_interval - 1) days to calculate cohort end_date
            # e.g. for 7 days cohort starting from 2015-01-01, end_date will be 2015-01-06
            cohort_end_date = cohort_start_date + timedelta(days=self._cohort_interval-1)

            # Let's create bucket dict for this cohort
            cohort_bucket_dict = self._create_cohort_bucket_dict(bucket_count)
            # Covert cohort start_date to str for the key
            result_key = cohort_start_date.strftime("%Y-%m-%d")
            cohort_result_dict[result_key] = {
                KEY_NUM_CUSTOMERS: 0,
                KEY_COHORT_LABEL: "{start} - {end}".format(
                    start=cohort_start_date.strftime("%Y/%m/%d"),
                    end=cohort_end_date.strftime("%Y/%m/%d")
                ),
                KEY_COHORT_BUCKETS: copy.deepcopy(cohort_bucket_dict)
            }

            # Decrease the bucket count for next cohort
            bucket_count -= 1

        return cohort_result_dict

    def process(self):
        """
        This method process customer and order csv file to
        generate cohort_analysis data.
        :return: Dict with cohort analysis data
        """
        # Load date from customer and order csv files
        customer_dict = self._load_customer_csv()
        order_dict = self._load_order_csv()
        for customer_id, signup_date in customer_dict.items():

            # Exclude customers who signed up before cohort start date or after the
            # cohort analysis last date
            if not (self._start_date <= signup_date <= self._cohort_last_date):
                continue
            # calculate days difference between signup date and cohort start date
            # so that we know to which cohort this customer belong to
            days_diff = (signup_date - self._start_date).days
            # Now calculate cohort index by dividing days_diff by cohort_interval
            # e.g. if cohort interval is 7 and days_diff is 32, cohort_index will be 4
            # which means this customer signed up in 5th cohort
            cohort_index = int(days_diff/self._bucket_interval)

            # Calculate cohort start_date by
            cohort_date = self._start_date + timedelta(days=cohort_index*self._bucket_interval)
            cohort_key = cohort_date.strftime("%Y-%m-%d")

            # update customer count for this cohort
            self._cohort_result[cohort_key][KEY_NUM_CUSTOMERS] += 1

            # Look in order_dict, if this customer has any order
            # if yes, then add to specific bucket using order date
            if customer_id in order_dict:
                order_date_list = order_dict[customer_id]
                self._update_cohort_buckets_for_orders(cohort_key, signup_date, order_date_list)

        return self._cohort_result

    def _update_cohort_buckets_for_orders(self, cohort_key, signup_date, order_date_list):
        sorted_order_date_list = sorted(order_date_list)
        first_order = True
        for order_date in sorted_order_date_list:
            days_diff_from_signup = (order_date - signup_date).days
            # Get cohort_bucket for this cohort
            cohort_buckets = self._cohort_result[cohort_key][KEY_COHORT_BUCKETS]
            # find the bucket number for this order
            cohort_bucket_num = floor(days_diff_from_signup/self._bucket_interval)
            # Make sure that this bucket exist in the cohort
            if cohort_bucket_num not in cohort_buckets:
                continue

            if first_order:
                cohort_buckets[cohort_bucket_num][KEY_FIRST_ORDERS] += 1
                first_order = False

            cohort_buckets[cohort_bucket_num][KEY_TOTAL_ORDERS] += 1

    def create_output_file(self):
        headline = ['Cohort', 'Customers']
        for bucket in range(self._num_buckets):
            bucket_header = "{start}-{end} days".format(
                start=bucket*self._cohort_interval,
                end=(bucket+1)*self._cohort_interval -1
            )
            headline.append(bucket_header)

        cohort_output_list = []
        # Now create output csv file
        with open(self._output_file_name, 'w', newline='') as csv_file:
            csv_writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE, quotechar='', escapechar='\\')
            csv_writer.writerow(headline)

            # Now fetch result from cohort_result and process them to write into csv file
            for cohort, data in self._cohort_result.items():
                cohort_row = []
                cohort_row.append(data[KEY_COHORT_LABEL])
                num_customers = data[KEY_NUM_CUSTOMERS]
                cohort_row.append(num_customers)

                bucket_result_list = []
                for bucket in range(self._num_buckets):
                    # Let's check if this bucket exists in the cohort
                    # If not, then just write NA
                    if bucket not in data[KEY_COHORT_BUCKETS]:
                        result_str = " NA "
                    else:
                        bucket_data = data[KEY_COHORT_BUCKETS][bucket]
                        if num_customers > 0:
                            percent_of_total_orders = \
                                (bucket_data[KEY_TOTAL_ORDERS]/num_customers) * 100
                            percent_of_first_orders = \
                                (bucket_data[KEY_FIRST_ORDERS]/num_customers) * 100
                        else:
                            percent_of_total_orders = 0
                            percent_of_first_orders = 0

                        result_str = "{0:.2f}% 1st time ({1}) {2:.2f}% total orderers ({3})".format(
                            percent_of_first_orders, bucket_data[KEY_FIRST_ORDERS],
                            percent_of_total_orders, bucket_data[KEY_TOTAL_ORDERS]
                        )
                    bucket_result_list.append(result_str)
                cohort_row.extend(bucket_result_list)
                cohort_output_list.append(cohort_row)
            # Write to CSV file
            csv_writer.writerows(cohort_output_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('start_date', help="Cohort starting date")
    parser.add_argument(
        '-ccfp',
        '--customer-csv-path',
        help="Full path of customer csv file (default is 'customer.csv')",
        default='customer.csv'
    )
    parser.add_argument(
        '-ocfp',
        '--order-csv-path',
        help="Full path of order csv file (default is 'order.csv')",
        default='order.csv'
    )
    parser.add_argument(
        '-ofn',
        '--output-file-name',
        help="Full path of output csv file (default is 'cohort_analysis.csv')",
        default="cohort_analysis.csv"
    )
    parser.add_argument(
        '-bi',
        '--bucket-interval',
        type=int,
        help="Cohort bucket interval (default is 7)",
        default=7
    )

    parser.add_argument(
        '-nc',
        '--num-cohort',
        type=int,
        help="Number of cohorts (should be at least 8)",
        default=8
    )

    parser.add_argument(
        '-tz',
        '--timezone',
        help="Timezone for grouping results (default is set to UTC)",
        default='UTC'
    )

    args = parser.parse_args()

    # validate num cohort, it should be equal or greater than 8
    if args.num_cohort < 8:
        raise ValueError("Incorrect number of cohort. Should be at least 8 or greater!")

    if not utils.valid_timezone(args.timezone):
        raise ValueError("Invalid timezone!")

    # Get date
    start_date = utils.get_datetime_from_date_str(args.start_date, timezone=args.timezone)

    # if path aren't passed let's set them to current dir
    current_dir_path = os.path.dirname(os.path.realpath(__file__))

    customer_csv_path = args.customer_csv_path if args.customer_csv_path \
        else current_dir_path + "\customer.csv"
    order_csv_path = args.order_csv_path if args.order_csv_path \
        else current_dir_path + "\order.csv"
    output_file_name = args.output_file_name if args.output_file_name \
        else current_dir_path + "\cohort_analysis.csv"

    cohort_analysis = CohortAnalysis(
        customer_csv_path=customer_csv_path,
        order_csv_path=order_csv_path,
        output_file_name=output_file_name,
        start_date=start_date,
        bucket_interval=args.bucket_interval,
        num_cohort= args.num_cohort,
        timezone=args.timezone
    )
    cohort_analysis.process()
    cohort_analysis.create_output_file()
