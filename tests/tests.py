import unittest
from collections import OrderedDict
from datetime import datetime, timedelta
from cohort_analysis import (CohortAnalysis, KEY_COHORT_BUCKETS, KEY_COHORT_LABEL,
                             KEY_FIRST_ORDERS, KEY_NUM_CUSTOMERS, KEY_TOTAL_ORDERS,
                             COHORT_BUCKET_COUNT)
from libs import utils


class TestCohortAnalysis(unittest.TestCase):
    def setUp(self):
        start_date = utils.get_datetime_from_date_str('2014-01-01')
        self.cohort_analysis = CohortAnalysis(
            customer_csv_path='tests/customer.csv',
            order_csv_path='tests/order.csv',
            start_date=start_date,
            output_file_name='tests/cohort_analysis.csv'
        )

    def test_initialize_cohort_result_dict_return_valid_ordered_dict(self):
        cohort_result = self.cohort_analysis._cohort_result
        num_cohort = len(cohort_result)
        # initialize_cohort should create 8 keys for cohort as
        # we have 56 days difference between start_date and end_date
        self.assertEqual(num_cohort, 8)
        # For first cohort the bucket count should be equal to 8
        self.assertEqual(
            len(cohort_result['2014-01-01'][KEY_COHORT_BUCKETS]),
            COHORT_BUCKET_COUNT
        )
        # For the last cohort the bucket count should be 1
        self.assertEqual(len(cohort_result['2014-02-19'][KEY_COHORT_BUCKETS]), 1)

        # cohort result should have correct label
        self.assertEqual(
            cohort_result['2014-01-01'][KEY_COHORT_LABEL],
            "2014/01/01 - 2014/01/07"
        )

    def test_load_customer_cvs_return_valid_dict(self):
        customer_dict = self.cohort_analysis._load_customer_csv()
        # customer_dict should be a OrderedDict
        self.assertTrue(isinstance(customer_dict, OrderedDict))
        # customer dict should have 10 customers info
        self.assertEqual(len(customer_dict), 10)
        # customer dict should have 1001 customer id as a key
        self.assertTrue('1001' in customer_dict)
        # customer dict shouldn't have 1011 customer id
        self.assertFalse('1011' in customer_dict)

    def test_load_order_csv_return_valid_dict(self):
        order_dict = self.cohort_analysis._load_order_csv()
        self.assertTrue(isinstance(order_dict, OrderedDict))
        self.assertEqual(len(order_dict), 6)
        # order dict should have information about customer_id 1004
        self.assertTrue('1004' in order_dict)
        # order dates should be stored as a list
        self.assertTrue(isinstance(order_dict['1004'], list))
        # order dict should have 3 orders from customer_id 1009
        self.assertTrue(len(order_dict['1004']), 3)

    def test_process_should_return_valid_cohort_dict(self):
        cohort_result = self.cohort_analysis.process()
        # cohort_result should be OrderedDict
        self.assertTrue(isinstance(cohort_result, OrderedDict))
        # total cohort should be 8
        self.assertEqual(len(cohort_result.keys()), 8)
        # for cohort of 2014-01-29 to 2014-02-04 there should be 3
        # new customers
        self.assertEqual(cohort_result['2014-01-29'][KEY_NUM_CUSTOMERS], 3)
        # As customer 1004 made 2 orders on 2014-01-25 and 2014-01-26
        # there should be 2 total_orders and 1 first_orders for 2014-01-15 cohort 2nd bucket
        self.assertEqual(
            cohort_result['2014-01-15'][KEY_COHORT_BUCKETS][1][KEY_FIRST_ORDERS],
            1
        )
        self.assertEqual(
            cohort_result['2014-01-15'][KEY_COHORT_BUCKETS][1][KEY_TOTAL_ORDERS],
            2
        )

    def test_cohort_end_date_should_match_given_timezone(self):
        start_date = utils.get_datetime_from_datetime_str('2014-01-01 02:02:22', 'Europe/Vienna')
        cohort_analysis = CohortAnalysis(
            customer_csv_path='tests/customer.csv',
            order_csv_path='tests/order.csv',
            start_date=start_date,
            output_file_name='tests/cohort_analysis.csv',
            timezone='Europe/Vienna'
        )
        expected_end_time = utils.get_datetime_from_datetime_str(
            '2014-02-26 02:02:22', 'Europe/Vienna'
        )
        self.assertEqual(cohort_analysis._cohort_last_date, expected_end_time)


if __name__ == '__main__':
    unittest.main()
