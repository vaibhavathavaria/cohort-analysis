import argparse
import csv
import os
import random
from datetime import timedelta
from libs import utils


def _generate_sorted_random_date_list(start_date, num_cohort, num_dates):
    date_list = []
    bucket_interval = 7
    span_over_days = num_cohort * bucket_interval

    for i in range(num_dates):
        random_days = random.randrange(span_over_days)
        random_hours = random.randrange(0,59)
        random_mins = random.randrange(0,59)
        random_secs = random.randrange(0,59)
        random_date = start_date + timedelta(
            days=random_days,
            hours=random_hours,
            minutes=random_mins,
            seconds=random_secs
        )
        date_list.append(random_date)
    date_list = sorted(date_list)
    return [x.strftime('%Y-%m-%d %H:%M:%S') for x in date_list]


def generate_customer_and_order_csv(start_date, num_cohort, num_customers=100):
    current_dir_path = os.path.dirname(os.path.realpath(__file__))
    # Generate customer csv
    headline = ['customer_id', 'signup_date']
    order_list = []

    with open(current_dir_path + '/customer.csv', 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
        csv_writer.writerow(headline)
        customer_id_list = [x for x in range(1, num_customers +1)]
        signup_date_list = _generate_sorted_random_date_list(start_date, num_cohort, num_customers)
        customer_list = list(zip(customer_id_list, signup_date_list))
        csv_writer.writerows(customer_list)

    # Generate order csv
    headline = ['customer_id', 'order_date']
    with open(current_dir_path + '/order.csv', 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file, quoting=csv.QUOTE_NONE)
        csv_writer.writerow(headline)
        for cust_info in customer_list:
            # Let's generate a random number between 0 and 5 and to
            # generate num of orders
            num_orders = random.randrange(5)
            if num_orders > 0:
                order_start_date = utils.get_datetime_from_datetime_str(cust_info[1])
                order_date_list = _generate_sorted_random_date_list(
                    order_start_date, num_cohort, num_orders
                )
                customer_id_list_for_order = [cust_info[0]] * num_orders
                customer_order_list = list(
                    zip(customer_id_list_for_order, order_date_list)
                )
                order_list.extend(customer_order_list)
        csv_writer.writerows(order_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('start_date', help="Start date for customer signup")
    parser.add_argument('-nc', '--num-cohort', type=int, help="Number of cohort", default=8)
    parser.add_argument(
        '-c', '--customers', type=int, help="Number of customers", default=100
    )

    args = parser.parse_args()
    start_date = utils.get_datetime_from_date_str(args.start_date)
    generate_customer_and_order_csv(
        start_date=start_date,
        num_cohort=args.num_cohort,
        num_customers=args.customers
    )
